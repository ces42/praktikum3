% !TeX spellcheck=en_US
\newcommand{\Na}{${}^{22}$Na\xspace}
\newcommand{\Co}{${}^{60}$Co\xspace}
\newcommand{\Cs}{${}^{137}$Cs\xspace}
\renewcommand{\K}{${}^{40}$K\xspace}
\section{Results}
A jupyter notebook containing the complete analysis can be found at \url{https://gitlab.com/ces42/praktikum3/blob/master/RAD/daten/Analyse.ipynb}.

\subsection{Calibration}

\begin{figure}[h]
    \begin{tikzpicture}
    \begin{axis}[
    height=1.0\plotheight, width=\plotwidth,
    xlabel={$E/\unit{MeV}$}, ylabel={channel},
        only marks,
        legend entries={\Na, \Co, \Cs, linear fit, affine fit, quadratic fit},
        legend style={at={(0.97,0.05)}, anchor=south east},
        xmin=.4, xmax=2.6
    ]
    \addplot+ [color=blue, mark=o, error bars/.cd, y dir=both, y explicit] table [x=E_Na, y=channel, y error=err, col sep=comma] {./daten/cali_Na.csv};
    \addplot+ [color=brown!70!black, mark=triangle, error bars/.cd, y dir=both, y explicit] table [x=E_Co, y=channel, y error=err, col sep=comma] {./daten/cali_Co.csv};
    \addplot+ [color=red!90!black, mark=square, error bars/.cd, y dir=both, y explicit] table [x=E_Cs, y=channel, y error=err, col sep=comma] {./daten/cali_Cs.csv};
    \addplot[domain=.45:2.55, color=green!80!black, smooth, thick, mark size=0, samples=2] {1000* .3447*x};
    \addplot[domain=.45:2.55, color=cyan, smooth, dashed, mark size=0, samples=2] {1000* .3275*x + 23.78};
    \addplot[domain=.45:2.55, color=magenta, smooth, dashed, mark size=0, samples=60] {1000^2* -0.00001275*x^2 + 1000* 0.3659*x};
    \end{axis}
    \end{tikzpicture}
    \caption{This diagram compares the (known) energy of $\gamma$ rays emitted from the calibration samples with the channel in which corresponding peak is centered. The best fit for a ``linear'' ($\text{channel} = A \cdot E$), an affine ($\text{channel} = A \cdot E + B$) and a ``quadratic'' model ($\text{channel} = A \cdot E + C \cdot E^2$) are shown. The error bars shown are the widths ($1 \sigma$) of the corresponding peaks.}
    \label{fig:cali}
\end{figure}

\Crefrange{fig:Na}{fig:Cs} shows the peaks in the spectra of the calibration samples. All peaks were fitted with Gaussians. \Cref{fig:cali} shows the calibration curve for the detector. The fact that the affine and the quadratic model fit better than the simple linear model suggests that there is systematic deviation from nonlinearity in the detector. We will however use the linear model $\text{channel} = \SI{.345}{keV^{-1}} \cdot E$ anyway.


\subsubsection{Exercise 2}
$29$ years after 1990, the activity of the \Cs-sample should have reduced to $\SI{171}{kBq}$. The cross-section of the second author can be approximated by a $\SI{1.8 x 0.30}{m}$ rectangle, which occupies a solid angle of $\SI{0.54}{sr}$ with respect to the sample. This results in the absorption of $\SI{25}{mJ}$ per year, which corresponds to \SI{0.41}{mSv}.

\subsection{Background}
\begin{figure}[h]
    \begin{tikzpicture}
    \begin{axis}[
    xmin=-0.05, xmax=3.080,
    height=1.0\plotheight, width=\plotwidth,
    xlabel={$E/\unit{MeV}$}, ylabel={events},
    % only marks,
    legend entries={with lead, without lead},
    legend style={at={(0.97,0.95)}, anchor=north east},
    ]
    \addplot+ [color=blue, thick, mark=,] table [x=E, y=bkg_blei, col sep=comma] {./daten/bkg.csv};
    \addplot+ [color=green, thick, mark=,] table [x=E, y=bkg_ohne, col sep=comma] {./daten/bkg.csv};
    \end{axis}
    \end{tikzpicture}
    \caption{Spectrum of background radiation ($\SI{600}{s}$) with and without lead shielding.}
    \label{fig:bkg}
\end{figure}
\Cref{fig:bkg} shows the spectrum of background radiation. The background (with lead) was subtracted from all recorded spectra shown in \cref{app}.

A total of $\SI{91}{k}$ events where detected when the lead shielding was removed. The detector crystal has a mass of $\SI{0.14}{kg}$. Assuming an average energy of $\SI{100}{keV}$ per photon (the actual average energy was $\SI{316}{keV}$ however) this would result in a dose of $\SI{0.54}{mSv}$. 

\subsection{Analysis of various samples}
\subsubsection{\ce{K2CO3}}
The spectrum of the sample (\cref{fig:K}) shows a peak at $\SI{1498 +- 29}{keV}$ (the uncertainty given here is the width ($1\sigma$) of the peak), near the actual $\SI{1461}{keV}$ \K-peak. The mass of the sample is $\SI{100.2}{g}$ which corresponds to $\SI{1.452}{mol}$ of Potassium. \K has a relative abundance%
\footnote{David R. Lide, Editor: Handbook of Chemistry and Physics, CRC Press, 90th Edition, p. 9-113} %
of $0.0117\%$, so there are $1.023\e{20}$ atoms of \K in the sample which results in an activity of $\SI{1802}{Bq}$. 

The peak in \cref{fig:K} Corresponds to $1227$ events or $2.045$ events per second. This is much lower than the activity of the \K since only a fraction of the gamma rays emitted travel through the detector and of those only some interact via the photoelectric effect (\cref{fig:K} suggests that a much larger number of photons interact via Compton scattering).

\subsubsection{Chernobyl sample}
Two peaks can be identified in the spectrum of the whey sample (\cref{fig:Chern}): At \SI{697 +- 17}{keV} and \SI{1472 +- 28}{keV}. The first peak corresponds to \Cs, the second on to \K.

Integrating the detected energy yields $\SI{2.63}{J s^{-1}}$, resulting in \SI{1.60}{\micro Sv} over the course of a week. This disregards the effects that reduced the number of (completely) detected electrons in the analysis of \ce{K2CO3}.

\subsubsection{Thorium}
The spectrum of Thorium is displayed in \cref{fig:Th}. Several peaks can be tentatively identified with $\gamma$ rays energies from decays in the Thorium series, as shown in \cref{tab:Th}. The systemic deviation is probably due to inaccurate calibration. The small (but well-isolated) peak at $\SI{1650}{keV}$ probably is the result of simultaneous detection of two photons. There seems to be a systemic deviation of $\SI{41}{keV}$ in these values. The peaks at $\SI{2160}{keV}$ and $\SI{1650}{keV}$ are discussed in \cref{fragen}.

\begin{figure}[h]
\renewcommand{\arraystretch}{1.3}
\centering
\begin{tabular}{c|c|c|c|c|c|c|c|c               }
    peak at $[\SI{}{keV}]$ & 267 & 371 & 555 & 628 & 777 & 967 & 1026 & 2641 \\
    \hline
    Isotope & \p{232}Th & \p{232}Ac & \p{208}Tl & \p{208}Tl & \p{212}Bi &\p{232}Th & \p{969}Th & \p{208}Tl \\
    actual energy $[\SI{}{keV}]$ & 239 & 339 & 511 & 583 & 727 & 911 & 969 & 2615 \\
\end{tabular}
\caption{Attempt at identifying the prominent peaks in \cref{fig:Th} with $\gamma$ ray energies from decays in the Thorium series.}
\label{tab:Th}
\end{figure}

\subsubsection{Uranium oxide}
The spectrum of Uranium is displayed in \cref{fig:U}. However the peaks do not seem to match the $\gamma$ ray energy values of decays in the Uranium series very well.

\subsubsection{Mushrooms}
The spectrum of the mushrooms (\cref{fig:shrooms}) shows a peak around $\SI{704}{keV}$ (channel $243$). This fits very closely to \Cs, which has a very prominent peak around channel $248$.

\subsection{Cosmic radiation}
\begin{figure}[h!]
    \begin{tikzpicture}
    \begin{axis}[
    height=.9\plotheight,
    ymin=0,
    enlarge x limits=false,
    %minor y tick num = 3,
    height=1.0\plotheight, width=\plotwidth,
    area style,
    scaled y ticks = false,
    y tick label style={/pgf/number format/fixed,},
    xlabel={$E/\unit{MeV}$}, ylabel={events per second},
    ]
    \addplot+[ybar interval, mark=no] table [x=bins, y=events] {./daten/hist_cosmic.csv};
    \end{axis}
    \end{tikzpicture}
    \caption{Histogram of the energies of detected cosmic $\gamma$ rays ($\SI{900}{s}$). One bin is $\SI{1.00}{GeV}$.}
    \label{fig:cosmic}
\end{figure}
Calibration suggests a relation $\text{channel} = \SI{.0198}{keV^{-1}} \cdot E$. \Cref{fig:cosmic} shows the energies of detected gamma rays. Integrating shows that the detector receives an energy of $\SI{0.15}{mJ}$ per year. This corresponds to an energy dose of $\SI{1.0}{mSv}$ per year.


